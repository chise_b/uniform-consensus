const emitter = require('../utils/emitter').getInstance().getEmitter();
const protos = require("../protos/model");
const service = require("./globalService").getInstance();
const decorateWithSingleton = require('../utils/decorateWithSingleton');

class EPFD {

    constructor() {
        //both alive and suspected should be a set of ProcessIds
        this.alive = new Set();
        this.suspected = new Set();
        this.delay = 30000;
        this.CONSTANT_DELAY = 30000;
        this.replay();
        this.heartBeatRequest();
        this.heartBeatReply();

    }

    heartBeatReply = () => {
        emitter.on('EPFD-HEARTBEATREPLY', message => {
            console.log('Received EPFD-HEARTBEATREPLY')
            //!!! We need to attach on heartbeatreply message the sender to know where to send the reply
            if (message.type === protos.Message.Type.EPFD_HEARTBEAT_REPLY){
                this.alive.add(message.sender)
            }
        })
    };

    heartBeatRequest = () => {
        emitter.on('EPFD-HEARTBEATREQUEST', receivedMessage => {
            console.log('Received EPFD-HEARTBEATREQUEST...')
            if (receivedMessage.type === protos.Message.Type.EPFD_HEARTBEAT_REQUEST) {
                let heartBeatReply = protos.EpfdHeartbeatReply_.create();
                let heartBeatReplyMessage = protos.Message.create();
                heartBeatReplyMessage.type = protos.Message.Type.EPFD_HEARTBEAT_REPLY;
                heartBeatReplyMessage.epfdHeartbeatReply_ = heartBeatReply;

                let plSend = protos.PlSend.create();
                plSend.message = heartBeatReplyMessage;
                //!!! We need to attach on heartbeatrequest message the sender to know where to send the reply
                plSend.destination = receivedMessage.sender;

                let message = protos.Message.create();
                message.type = protos.Message.Type.PL_SEND;
                message.plSend = plSend;

                emitter.emit('PL-SEND', message);

            }
        })
    };

    sendHeartBeatRequest = (process) => {
        let heartBeatRequest = protos.EpfdHeartbeatRequest_.create();
        let heartBeatRequestMessage = protos.Message.create();
        heartBeatRequestMessage.type = protos.Message.Type.EPFD_HEARTBEAT_REQUEST;
        heartBeatRequestMessage.epfdHeartbeatRequest_ = heartBeatRequest;

        let plSend = protos.PlSend.create();
        plSend.message = heartBeatRequestMessage;
        plSend.destination = process;

        let message = protos.Message.create();
        message.type = protos.Message.Type.PL_SEND;
        message.plSend = plSend;

        console.log('Sending HEARTBEATREQUEST...')
        emitter.emit('PL-SEND', message);
    };

    replay = (previousTimeoutId) => {

        if (previousTimeoutId) {
            clearTimeout(previousTimeoutId);
        }
        const timeoutId = setTimeout(() => {
            console.log('Received EPFD-TIMEOUT...')
            let aliveAndSuspectedIntersection = new Set(
                [...this.alive].filter(x => this.suspected.has(x)));

            if (aliveAndSuspectedIntersection.size !== 0) {
                this.delay += this.CONSTANT_DELAY;
            }

            service.getAllProcesses().forEach(process => {
                if (!this.alive.has(process) && !this.suspected.has(process)) {
                    this.suspected.add(process);

                    let message = protos.Message.create();
                    message.type = protos.Message.Type.EPFD_SUSPECT;

                    message.epfdSuspect = protos.EpfdSuspect.create();
                    message.epfdSuspect.process = process;

                    emitter.emit('EPFD-SUSPECT', message);
                } else if (this.alive.has(process) && this.suspected.has(process)) {
                    this.suspected.delete(process);

                    let message = protos.Message.create();
                    message.type = protos.Message.Type.EPFD_RESTORE;

                    message.epfdRestore = process.EpfdRestore.create();
                    message.epfdRestore.process = process;

                    emitter.emit('EPFD-RESTORE', message);
                }

                this.sendHeartBeatRequest(process);

            });

            this.alive.clear();
            this.replay(timeoutId);
        }, this.delay);
    }


}

module.exports = decorateWithSingleton(EPFD)
