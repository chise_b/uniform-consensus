const emitter = require('../utils/emitter').getInstance().getEmitter();
const service = require("./globalService").getInstance();
const _ = require('lodash');
const decorateWithSingleton = require('../utils/decorateWithSingleton');
const protos = require("../protos/model");

class EC {

    constructor() {
        this.trusted = service.getLeader();
        this.lastts = 0;
        this.ts = service.getCurrentProcess().rank;

        this.ecTrust();
        this.bebDeliverNewEpoch();
        this.plDeliverNack();
    }

    plDeliverNack = () => {
        emitter.on('PL-DELIVER-NACK', message => {
            console.log('Received PL-DELIVER-NACK...')
            const nackMessage = message.plDeliver.message;
            if (nackMessage.type == protos.Message.Type.EC_NACK_) {
                const currentProcesss = service.getCurrentProcess();

                //because we are on the same machine we only compare ports
                if (this.trusted.port ===currentProcesss.port) {
                    this.newEpoch();
                }
            }
        })
    }

    startEpoch = (newts, sender) => {
        let newEpoch = protos.EcNewEpoch_.create();
        newEpoch.timestamp = newts;
        newEpoch.newLeader = sender;

        let message = protos.Message.create();
        message.type = protos.Message.Type.EC_START_EPOCH;
        message.ecStartEpoch = newEpoch;

        console.log('Sending EC-STARTEPOCH...')
        emitter.emit('EC-STARTEPOCH', message);
    }

    sendNack = (sender) => {
        console.log('Sending EC-NACK...')
        let nack = protos.EcNack_.create();

        let nackMessage = protos.Message.create();
        nackMessage.type = protos.Message.Type.EC_NACK_;
        nackMessage.ecNack_ = nack;

        let plSend = protos.PlSend.create();
        plSend.message = nackMessage;
        plSend.destination = sender;

        let message = protos.Message.create();
        message.type = protos.Message.Type.PL_SEND;
        message.plSend = plSend;

        emitter.emit('PL-SEND', message);
    }

    bebDeliverNewEpoch = () => {
        emitter.on('BEB-DELIVER-NEWEPOCH', message => {
            console.log('Received BEB-DELIVER-NEWEPOCH...')
            //sender=l
            const sender = message.bebDeliver.sender;
            const bebDeliverMessage = message.bebDeliver.message;
            if (bebDeliverMessage.type === protos.Message.Type.EC_NEW_EPOCH_) {
                const newts = bebDeliverMessage.ecNewEpoch_.timestamp;
                //because we work in the same hub we only compare ports
                if (this.trusted.port === sender.port && newts > this.lastts) {
                    this.lastts = newts;
                    this.startEpoch(newts, sender)
                } else {
                    this.sendNack(sender);
                }
            }
        })
    }

    newEpoch = () => {
        const nrOfProcesses = service.getAllProcesses().length;
        this.ts += nrOfProcesses;

        let newEpoch = protos.EcNewEpoch_.create();
        newEpoch.timestamp = this.ts;

        let message = protos.Message.create();
        message.type = protos.Message.Type.EC_NEW_EPOCH_;
        message.ecNewEpoch_ = newEpoch;

        console.log('Sending BEB-BROADCAST - EC-NEWEPOCH...')
        emitter.emit('BEB-BROADCAST', message)
    }

    ecTrust = () => {
        emitter.on('ELD-TRUST', message => {
            console.log('Received ELD-TRUST')
            const process = message.eldTrust.process;
            this.trusted = process;
            const currentProcess = service.getCurrentProcess();

            //because we are on the same machine we only compare ports
            if (process.port === currentProcess.port) {
                this.newEpoch()
            }
        });
    }
}

module.exports = decorateWithSingleton(EC)
