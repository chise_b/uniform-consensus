const emitter = require('../utils/emitter').getInstance().getEmitter();
const protos = require("../protos/model");
const service = require("./globalService").getInstance();
const _ = require('lodash');
const decorateWithSingleton = require('../utils/decorateWithSingleton');

class ELD {

    constructor() {
        this.suspected = new Set();
        this.leader = null;
        this.eldSuspect();
        this.eldRestore();
        this.setLeader();
    }

    setLeader = () => {
        let allProcesses = service.getAllProcesses();
        let notSuspectedProcesses = new Set([...allProcesses].filter(x => !this.suspected.has(x)))
        const currentLeader = _.maxBy(Array.from(notSuspectedProcesses), 'rank');
        //because we work on the same hub we only compare ports
        if (currentLeader && (this.leader == null || this.leader.port !== currentLeader.port)) {
            this.leader = currentLeader;

            let eldTrust = protos.EldTrust.create();
            eldTrust.process = currentLeader;

            let message = protos.Message.create()
            message.type = protos.Message.Type.ELD_TRUST;
            message.eldTrust = eldTrust;

            service.setLeader(currentLeader);
            console.log('Sending ELD-TRUST...')
            emitter.emit('ELD-TRUST', message)
        }
    };

    eldRestore = () => {
        emitter.on('EPFD-RESTORE', message => {
            console.log('Received EPFD-RESTORE...')
            if (message.type == protos.Message.Type.EPFD_RESTORE) {
                const process = message.epfdSuspect.process;
                this.suspected.delete(process);
                this.setLeader()
            }
        });
    };

    eldSuspect = () => {
        emitter.on('EPFD-SUSPECT', message => {
            console.log('Received EPFD-SUSPECT...')
            if (message.type == protos.Message.Type.EPFD_SUSPECT) {
                const process = message.epfdSuspect.process;
                this.suspected.add(process);
                this.setLeader()
            }
        });
    }


}

module.exports = decorateWithSingleton(ELD);
