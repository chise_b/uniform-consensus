const emitter = require('../utils/emitter').getInstance().getEmitter();
const ep = require('./ep');
const protos = require("../protos/model");
const service = require("./globalService").getInstance();
const _ = require('lodash');
const decorateWithSingleton = require('../utils/decorateWithSingleton');
const state = require('./state')

class UC {

    constructor() {
        this.val = null;
        this.proposed = false;
        this.decided = false;
        this.ets = 0;
        //TODO get the leader
        this.l = service.getLeader();
        this.ep = new ep.EP(0, new state.State(0, null));
        this.newts = 0;
        this.newl = null;

        this.ucPropose();
        this.ecStartEpoch();
        this.epAborted();
        this.epDecide();
        this.hubDecide();
    }

    ucPropose = () => {
        emitter.on('UC-PROPOSE', message => {
            console.log('Received UC-PROPOSE')
            if (message.type === protos.Message.Type.UC_PROPOSE) {
                const value = message.ucPropose.value;
                this.val = value;
            }
        })
    }

    ecStartEpoch = () => {
        emitter.on('EC-STARTEPOCH', receivedMessage => {
            if (receivedMessage.type === protos.Message.Type.EC_START_EPOCH) {
                const ecStartEpoch = receivedMessage.ecStartEpoch;
                this.newts = ecStartEpoch.newTimestamp;
                this.newl = ecStartEpoch.newLeader;

                let epAbort = protos.EpAbort.create();

                let message = protos.Message.create();
                message.type = protos.Message.Type.EP_ABORT;
                message.epAbort = epAbort;

                console.log('Sending EP-ABORT...')
                emitter.emit('EP-ABORT', message);
            }
        })
    }

    epAborted = () => {
        emitter.on('EP-ABORTED', receivedMessage => {
            console.log('Received EP-ABORTED...')
            if (receivedMessage.type == protos.Message.Type.EP_ABORTED) {
                let epAborted = receivedMessage.epAborted;
                if (this.ep.ets === epAborted.ets) {
                    this.ets = epAborted.ets;
                    this.l = this.newl;
                    this.proposed = false;

                    let value = null;
                    if (epAborted.value && epAborted.value.v) {
                        value = epAborted.value.v
                    }
                    const currentState = new state.State(epAborted.valueTimestamp, value);

                    this.ep = new ep.EP(epAborted.ets);
                    this.ep.state = currentState;
                    service.setLeader(this.l);
                    this.checkPropose();
                }
            }
        })
    }

    checkPropose = () => {
        const currentProcess = service.getCurrentProcess();
        //because we are on the same machine we only compare ports
        if (currentProcess.port === this.l.port && this.val != null && this.proposed === false) {
            this.proposed = true;

            const value = protos.Value.create();
            value.v = this.val;

            let epPropose = protos.EpPropose.create();
            epPropose.value = value;

            let message = protos.Message.create();
            message.type = protos.Message.Type.EP_PROPOSE;
            message.epPropose = epPropose;


            emitter.emit('EP-PROPOSE', message);
        }
    }

    epDecide = () => {
        emitter.on('EP-DECIDE', receivedMessage => {
            console.log('Received EP-DECIDE...')
            const epDecide = receivedMessage.epDecide;

            if (this.ep.ets === epDecide.ets && this.decided === false) {
                this.decided = true;

                let ucDecide = protos.UcDecide.create();
                ucDecide.value = epDecide.value;

                let message = protos.Message.create();
                message.type = protos.Message.Type.UC_DECIDE;
                message.ucDecide = ucDecide;

                emitter.emit('UC-DECIDE', message);
            }
        })
    }

    hubDecide = () => {
        emitter.on('UC-DECIDE', receivedMessage => {

            const currentProcess = service.getCurrentProcess();

            const ucDecide = receivedMessage.ucDecide;

            let appDecide = protos.AppDecide.create();
            appDecide.value = ucDecide.value;

            let innerMessage = protos.Message.create();
            innerMessage.type = protos.Message.Type.APP_DECIDE;
            innerMessage.appDecide = appDecide;

            let networkMessage = protos.NetworkMessage.create();
            networkMessage.senderHost = '127.0.0.1';
            networkMessage.senderListeningPort = currentProcess.port;
            networkMessage.message = innerMessage;

            let message = protos.Message.create();
            message.type = protos.Message.Type.NETWORK_MESSAGE;
            message.networkMessage = networkMessage;

            const net = require('net');
            const client = new net.Socket();

            client.connect(5000, '127.0.0.1', () => {
                console.log('Sending decide to the hub!');

                const buffer = service.encode(message);

                console.log('Sending APP-REGISTRATION...')

                client.write(buffer);
                client.end();

            });
        })
    }
}

module.exports = decorateWithSingleton(UC);
