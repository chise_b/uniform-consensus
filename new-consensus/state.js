
class State{

    constructor(valts, val) {
        this.valts = valts;
        this.val = val;
    }
}

module.exports = {
    State
}
