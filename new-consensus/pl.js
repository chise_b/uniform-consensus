const emitter = require('../utils/emitter').getInstance().getEmitter();
const protos = require("../protos/model");
const service = require("./globalService").getInstance();
const net = require('net');
const decorateWithSingleton = require('../utils/decorateWithSingleton');

class PL {

    constructor() {
        this.plSend();
        this.plDeliver();
    }


    createBebDeliverMessage = (message, sender) =>{
        let bebDeliver = protos.BebDeliver.create();
        bebDeliver.sender = sender;
        bebDeliver.message = message;

        let bebDeliverMessage = protos.Message.create();
        bebDeliverMessage.type = protos.Message.Type.BEB_DELIVER;
        bebDeliverMessage.bebDeliver = bebDeliver;

        return bebDeliverMessage;
    }

    // BEB-DELIVER-READ - ep
    // BEB-DELIVER-WRITE - ep
    // BEB-DELIVER-DECIDED - ep
    // PL-DELIVER-STATE - ep
    // PL-DELIVER-ACCEPT - ep
    // BEB-DELIVER-NEWEPOCH - ec
    // PL-DELIVER-NACK - ec

    plDeliver = () => {
        emitter.on('PL-DELIVER', receivedMessage => {
            console.log('Received PL-DELIVER...')
            const plDeliver = receivedMessage.plDeliver;
            const message = plDeliver.message;
            const sender = plDeliver.sender;

            let bebDeliverMessage = this.createBebDeliverMessage(message, sender);

            if (message.type === protos.Message.Type.EP_READ_){
                emitter.emit('BEB-DELIVER-READ', bebDeliverMessage);
            }
            else if(message.type === protos.Message.Type.EP_WRITE_){
                emitter.emit('BEB-DELIVER-WRITE', bebDeliverMessage);
            }
            else if(message.type === protos.Message.Type.EP_DECIDED_){
                emitter.emit('BEB-DELIVER-DECIDED', bebDeliverMessage);
            }
            else if(message.type === protos.Message.Type.EP_STATE_){
                emitter.emit('PL-DELIVER-STATE', bebDeliverMessage);
            }
            else if(message.type === protos.Message.Type.EP_ACCEPT_){
                emitter.emit('PL-DELIVER-ACCEPT', bebDeliverMessage);
            }
            else if(message.type === protos.Message.Type.EC_NEW_EPOCH_){
                emitter.emit('BEB-DELIVER-NEWEPOCH', bebDeliverMessage);
            }
            else if(message.type === protos.Message.Type.EC_NACK_){
                emitter.emit('PL-DELIVER-NACK', bebDeliverMessage);
            }
            else if (message.type === protos.Message.Type.EPFD_HEARTBEAT_REQUEST){
                let epHeartBeatMessage = protos.Message.create();
                epHeartBeatMessage.type = protos.Message.Type.EPFD_HEARTBEAT_REQUEST;
                epHeartBeatMessage.sender = plDeliver.sender;
                setTimeout(() =>{
                    emitter.emit('EPFD-HEARTBEATREQUEST', epHeartBeatMessage);
                }, 30000)
            }
            else if (message.type === protos.Message.Type.EPFD_HEARTBEAT_REPLY){
                let epHeartBeatMessage = protos.Message.create();
                epHeartBeatMessage.type = protos.Message.Type.EPFD_HEARTBEAT_REPLY;
                epHeartBeatMessage.sender = plDeliver.sender;
                emitter.emit('EPFD-HEARTBEATREPLY', epHeartBeatMessage)
            }
            else{
                console.log('HERE with ' + message.type)
            }


        })
    };

    plSend = () => {
        emitter.on('PL-SEND', receivedMessage => {
            console.log('Received PL-SEND...')
            let plSend = receivedMessage.plSend;
            const destination = plSend.destination;

            let insideMessage = plSend.message;

            let networkMessage = protos.NetworkMessage.create();
            networkMessage.message = insideMessage;
            networkMessage.senderListeningPort = process.env.PORT;

            let message = protos.Message.create();
            message.type = protos.Message.Type.NETWORK_MESSAGE;
            message.networkMessage = networkMessage;
            message.systemId = 'sys-1';

            const client = new net.Socket();
            client.connect(destination.port, '127.0.0.1', () => {
                console.log('Trying to PL-SEND to client with message: ' + JSON.stringify(message));

                const buffer = service.encode(message);

                client.write(buffer);
                client.end();

            });

        })
    };
}

module.exports = decorateWithSingleton(PL)
