const service = require("./globalService").getInstance();
const emitter = require('../utils/emitter').getInstance().getEmitter();
const protos = require("../protos/model");
const decorateWithSingleton = require('../utils/decorateWithSingleton');

class BEB{

    constructor() {
        this.bebBroadcast()
    }

    bebBroadcast = () =>{
        emitter.on('BEB-BROADCAST', receivedMessage =>{
            console.log('Receiving BEB-BROADCAST...')
            service.getAllProcesses().forEach(process =>{

                let plSend = protos.PlSend.create();
                plSend.message = receivedMessage;
                plSend.destination = process;

                let message = process.Message.create();
                message.type = process.Message.Type.PL_SEND;
                message.plSend = plSend;

                emitter.emit('PL-SEND', message);
            })
        })

    }
}

module.exports = decorateWithSingleton(BEB)
