const emitter = require('../utils/emitter').getInstance().getEmitter();
const protos = require("../protos/model");
const service = require("./globalService").getInstance();
const decorateWithSingleton = require('../utils/decorateWithSingleton');

const net = require('net');
const client = new net.Socket();

const UC = require('./uc')
const PL = require('./pl')
const EPFD = require('./epfd')
const ELD = require('./eld')
const EC = require('./ec')
const BEB = require('./beb')

class APP {

    constructor() {
    }

    appPropose = (networkMessage) => {
        const appPropose = networkMessage.message.appPropose;
        service.setAllProcesses(appPropose.processes);

        this.initalizeAll()

        let ucPropose = protos.UcPropose.create();
        ucPropose.value = appPropose.value;

        let message = protos.Message.create();
        message.type = protos.Message.Type.UC_PROPOSE;
        message.ucPropose = ucPropose;

        console.log('Sending UC-PROPOSE...')
        emitter.emit('UC-PROPOSE', message);

    }

    initalizeAll = () =>{
        PL.getInstance();
        ELD.getInstance();
        EPFD.getInstance();
        BEB.getInstance();
        EC.getInstance();
        UC.getInstance()

    }

    appRegistration = (owner, index) => {
        let appRegistration = protos.AppRegistration.create();
        appRegistration.owner = owner;
        appRegistration.index = index;

        let appRegistrationMessage = protos.Message.create();
        appRegistrationMessage.type = protos.Message.Type.APP_REGISTRATION;
        appRegistrationMessage.appRegistration = appRegistration;

        let networkMessage = protos.NetworkMessage.create();
        networkMessage.senderListeningPort = 5004;
        networkMessage.message = appRegistrationMessage;

        let message = protos.Message.create();
        message.type = protos.Message.Type.NETWORK_MESSAGE;
        message.networkMessage = networkMessage;


        client.connect(5000, '127.0.0.1', () => {
            console.log('Connected to the hub!');

            const buffer = service.encode(message);

            console.log('Sending APP-REGISTRATION...')

            client.write(buffer);
            client.end();

        });
    }

}

module.exports = decorateWithSingleton(APP);
