const decorateWithSingleton = require('../utils/decorateWithSingleton');
const protos = require("../protos/model");

class GlobalService {

    constructor() {
        this.allProcesses = null;
        this.leader = null;
    }

    setAllProcesses = (processes) => {
        this.allProcesses = processes;
    };

    getAllProcesses = () => {
        return this.allProcesses;
    };

    getLeader = () => {
        return this.leader;
    };

    setLeader = (leader) => {
        if (!this.leader) {
            this.leader = leader;
        }
    };

    encode = (networkMessage) =>{
        var bufferedData = protos.Message.encode(networkMessage).finish();
        var messageBytes = Uint32Array.from(bufferedData);

        var messageLength = Buffer.alloc(4);
        messageLength.writeUInt32BE(messageBytes.length);

        var arr = new Uint32Array(4 + messageBytes.length);
        arr.set(messageLength);
        arr.set(messageBytes, 4);

        var buffer = Buffer.from(arr);

        return buffer;
    }

    decode = (buffer) =>{
        var message = Buffer.from(buffer);
        var newMessage = message.subarray(4);
        try{
            return protos.Message.decode(newMessage);
        }catch(err){
            return {}
        }
    }

    getCurrentProcess = () => {
        let processId = protos.ProcessId.create();
        processId.index = 1;
        processId.rank = process.env.RANK;
        processId.port = process.env.PORT;
        processId.owner = process.env.OWNER;
        processId.index = process.env.HOST;

        return processId;
    }


}

module.exports = decorateWithSingleton(GlobalService);
