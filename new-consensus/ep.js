const emitter = require('../utils/emitter').getInstance().getEmitter();
const protos = require("../protos/model");
const service = require("./globalService").getInstance();
const _ = require('lodash');
const state = require('./state');
class EP {



    constructor(ets, state) {
        this.state = state;
        this.tmpval = null;
        this.states = [];
        this.accepted = 0;
        this.ets = ets;

        this.epPropose();
        this.bebDeliverRead();
        this.plDeliverState();
        this.bebDeliverWrite();
        this.plDeliverAccept();
        this.bebDeliverDecided();
        this.epAbort();
    }

    epPropose = () => {
        emitter.on('EP-PROPOSE', receivedMessage => {
            console.log('Received EP-PROPOSE...')
            const currentProcess = service.getCurrentProcess();
            const currentLeader = service.getLeader();

            //because we are on the same machine we only compare ports
            if (currentLeader.port === currentProcess.port) {

                const epPropose = receivedMessage.epPropose;
                const value = epPropose.value;

                this.tmpval = value.v;

                let epRead = protos.EpRead_.create();

                let message = protos.Message.create();
                message.type = protos.Message.Type.EP_READ_;

                message.epRead_ = epRead;

                emitter.emit('BEB-BROADCAST', message);
            }
            //TODO check if we need this if
            // if (!value.decided){
            //
            // }
        })
    }

    bebDeliverRead = () => {
        emitter.on('BEB-DELIVER-READ', receivedMessage => {
            console.log('Received BEB-DELIVER-READ...')
            const bebDeliver = receivedMessage.bebDeliver;
            const sender = bebDeliver.sender;

            let epState = protos.EpState_.create();
            epState.value = this.state.val;
            epState.valueTimestamp = this.state.valts;

            let epStateMessage = protos.Message.create();
            epStateMessage.type = protos.Message.Type.EP_STATE_;
            epStateMessage.epState_ = epState;

            let plSend = protos.PlSend.create();
            plSend.destination = sender;
            plSend.message = epStateMessage;

            let message = protos.Message.create();
            message.type = protos.Message.Type.PL_SEND;
            message.plSend = plSend;

            emitter.emit('PL-SEND', message);
        })
    }

    plDeliverState = () => {
        emitter.on('PL-DELIVER-STATE', receivedMessage => {
            console.log('Received PL-DELIVER-STATE')
            const plDeliver = receivedMessage.plDeliver;
            const sender = plDeliver.sender;
            const currentLeader = service.getLeader();
            const epState = plDeliver.message.epState_;

            //because we are on the same machine we only compare ports
            if (sender.port === currentLeader.port) {
                this.states[q] = new state.State(epState.valueTimestamp, epState.value.v);

                const nrOfProcesses = service.getAllProcesses().length;

                if (this.state.length > nrOfProcesses / 2) {
                    const highestStateByTimeStamp = _.maxBy(this.states, 'valts');
                    if (highestStateByTimeStamp.val != null) {
                        this.tmpval = highestStateByTimeStamp.val;
                    }

                    this.states = [];
                    this.sendBroadcastWrite();
                }

            }
        })
    }

    sendBroadcastWrite = () => {
        let epWrite = protos.EpWrite_.create();
        epWrite.value = this.tmpval;

        let epWriteMessage = protos.Message.create();
        epWriteMessage.type = protos.Message.Type.EP_WRITE_;
        epWriteMessage.epWrite_ = epWrite;

        let bebBroadcast = protos.BebBroadcast.create();
        bebBroadcast.message = epWriteMessage;

        let message = protos.Message.create();
        message.type = protos.Message.Type.BEB_BROADCAST;
        message.bebBroadcast = bebBroadcast;

        console.log('Sending BEB-BROADCAST-WRITE')
        emitter.emit('BEB-BROADCAST', message);

    }

    bebDeliverWrite = () => {
        emitter.on('BEB-DELIVER-WRITE', receivedMessage => {
            console.log('Received BEB-DELIVER-WRITE...')
            const bebDeliver = receivedMessage.bebDeliver;
            if (bebDeliver.message.type === protos.Message.Type.EP_WRITE_) {
                const epWrite = bebDeliver.message.epWrite_;
                //sender = l
                const sender = bebDeliver.sender;

                //we changed here
                this.state = new state.State(this.ets, epWrite.value.v);

                let epAccept = protos.EpAccept_.create();

                let epAcceptMessage = protos.Message.create();
                epAcceptMessage.type = protos.Message.Type.EP_ACCEPT_;
                epAcceptMessage.epAccept_ = epAccept;

                let plSend = protos.PlSend.create();
                plSend.message = epAcceptMessage;
                plSend.destination = sender;

                let message = protos.Message.create();
                message.type = protos.Message.Type.PL_SEND;
                message.plSend = plSend;

                emitter.emit('PL-SEND', message);

            }

        })
    }

    plDeliverAccept = () => {
        emitter.on('PL-DELIVER-ACCEPT', receivedMessage => {
            console.log('Received PL-DELIVER-ACCEPT...')
            const currentProcess = service.getCurrentProcess();
            const currentLeader = service.getLeader();

            //because we are one the same machine we only compare ports
            if (currentLeader.port === currentProcess.port) {
                const plDeliver = receivedMessage.plDeliver;
                if (plDeliver.message.type == protos.Message.Type.EP_ACCEPT_) {
                    this.accepted += 1

                    const nrOfProcesses = service.getAllResponseHeaders().length;

                    if (this.accepted > nrOfProcesses / 2) {
                        this.accepted = 0;
                        this.bebBroadcastDecided();
                    }
                }
            }
        })
    }

    bebBroadcastDecided = () => {
        let value = protos.Value.create();
        value.v = this.tmpval;

        let decided = protos.EpDecided_.create();
        decided.value = value;

        let epDecidedMessage = protos.Message.create();
        epDecidedMessage.type = protos.Message.Type.EP_DECIDED_;
        epDecidedMessage.epDecided_ = decided;

        let bebBroadcast = protos.BebBroadcast.create();
        bebBroadcast.message = epDecidedMessage;

        let message = protos.Message.create();
        message.type = protos.Message.Type.EP_DECIDED_;
        message.bebBroadcast = bebBroadcast;

        console.log('Sending BEB-BROADCAST...')
        emitter.emit('BEB-BROADCAST', message);

    }

    bebDeliverDecided = () => {
        emitter.on('BEB-DELIVER-DECIDED', receivedMessage => {
            console.log('Received BEB-DELIVER-DECIDED...')
            const bebDeliver = receivedMessage.bebDeliver;
            const sender = bebDeliver.sender;

            if (bebDeliver.message.type === protos.Message.Type.EP_DECIDED_) {
                const value = bebDeliver.message.epDecided_.value;

                let epDecide = protos.EpDecide.create();
                epDecide.ets = this.ets;
                epDecide.value = value;

                let message = protos.Message.create();
                message.type = protos.Message.Type.EP_DECIDE;
                message.epDecide = epDecide;

                emitter.emit('EP-DECIDE', message)
            }

        })
    }

    epAbort = () => {
        emitter.on('EP-ABORT', receivedMessage => {
            console.log('Received EP-ABORT...')
            if (receivedMessage.type == protos.Message.Type.EP_ABORT) {
                let epAborted = protos.EpAborted.create();
                epAborted.value = this.state.val;
                epAborted.valueTimestamp = this.state.valts;
                epAborted.ets = this.ets;

                let message = protos.Message.create();
                message.type = protos.Message.Type.EP_ABORTED;
                message.epAborted = epAborted;

                emitter.emit('EP-ABORTED', message);
            }
        })
    }


}

module.exports = {
    EP
}
