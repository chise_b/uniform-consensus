const commonOperationsHandler = require('../../utils/commonOperationsHandler');

const eldRecoveryHandler = require('./eld/eldRecovery');
const plDeliverHandler = require('./eld/eldDeliver');

module.exports = (data) => {
    const commonOperations = commonOperationsHandler();

    const eldRecoveryOperations = eldRecoveryHandler(commonOperations);
    const plDeliverOperations = plDeliverHandler(commonOperations);

    eldRecoveryOperations.handle(data);
    plDeliverOperations.handle(data);
};
