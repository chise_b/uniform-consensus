const continueOnEvent = require('../../../utils/continueOnEvent');
const Operations = require('../../../utils/Operations');
const eldService = require('./eldService').getInstance();
const protos = require("../../../protos/model");

module.exports = (baseOperations) => {
    const operations = Operations.fromOperations(baseOperations);
    operations.use(continueOnEvent({
        type: 'PL_DELIVER',
    }));
    operations.use(async (context, next) =>{
        const message = protos.Message.create(context.body);
        const candidate = {
            process: message.plDeliver.sender,
            epoch: message.plDeliver.message.eldHeartbeat_.epoch,
        };
        const searchedCandidate = eldService.existsCandidate(candidate);
        if(searchedCandidate){
            eldService.removeCanidates(searchedCandidate);
        }
        eldService.addCandidate(candidate);
        next();
    });

    return operations;
};
