const continueOnEvent = require('../../../utils/continueOnEvent');
const Operations = require('../../../utils/Operations');
const eldService = require('./eldService').getInstance();
const protos = require("../../../protos/model");

module.exports = (baseOperations) => {
    const operations = Operations.fromOperations(baseOperations);
    operations.use(continueOnEvent({
        type: 'ELD_RECOVERY',
    }));
    operations.use(async (context, next) => {

        // const leader = eldService.getProcessWithMaxRank();
        //TODO remove this mockup leader
        let leader = protos.ProcessId.create()
        leader.host = "127.0.0.2";
        leader.index = 1;
        leader.owner = 'Bogdan Chise';
        leader.port = 8080;
        leader.rank = 80;

        //we need this for timeout
        context.body.leader = leader;

        eldService.sendTrustLeader(leader);

        next();
    });
    operations.use(async (context, next) => {
        let epoch = eldService.retrieve();
        epoch += 1;
        eldService.store(epoch);
        eldService.sendHeartbeatToAllProcesses(epoch);
        eldService.removeAllCandidates();

        next();
    });
    operations.use(async (context, next) => {
        eldService.replay(context.body.leader);
        next();
    });

    return operations;
};

