const fs = require('fs');
const net = require('net');
const protos = require("../../../protos/model");
const decorateWithSingleton = require('../../../utils/decorateWithSingleton');
const _ = require('lodash');

class EldService {

    constructor() {
        this.epoch = 0;
        //TODO use relative path
        this.filePath = "/home/bogdan/Desktop/Faculta/master/An1/sem2/irsdp/project/store/eld.txt";
        this.store(this.epoch);
        this.candidates = new Set();
        this.processes = [];
        this.delay = 30000;
        this.CONSTANT_DELAY = 30000;
    }

    emitRecovery = () => {
        const client = new net.Socket();

        client.connect(process.env.PORT, process.env.HOST, () => {

            let message = protos.Message.create();
            message.type = protos.Message.Type.ELD_RECOVERY;
            const bufferedData = Buffer.from(JSON.stringify(message));
            client.write(bufferedData, (err) => {

                // don't use destroy on client see
                // https://stackoverflow.com/questions/9191587/how-to-disconnect-from-tcp-socket-in-nodejs
                client.end();
            });
        });
    };

    store = (epoch) => {
        fs.writeFileSync(this.filePath, epoch);
    };

    retrieve = () => {
        return +fs.readFileSync(this.filePath);
    };

    getProcessWithMaxRank = () => {
        return this.processes.reduce((p, c) => p.rank > c.rank ? p : c);
    };

    getAllProcesses = () => {
        return this.processes;
    };

    removeCanidates = toRemoveCandidates =>{
        //TODO check if remove works
        toRemoveCandidates.forEach(toRemoveCandidate => this.candidates.delete(toRemoveCandidate));
    };

    removeAllCandidates = () =>{
        this.candidates = new Set();
    };

    addCandidate = candidate => {
        this.candidates.add(candidate);
    };

    existsCandidate = (candidate) =>{
        let arrayCandidates = Array.from(this.candidates.values());
        return arrayCandidates.find(fcandidate => _.isMatch(fcandidate, candidate.process) && fcandidate.epoch < candidate.epoch)
    };

    getDelay() {
        return this.delay;
    }

    sendHeartbeatToAllProcesses = (epoch) => {
        const client = new net.Socket();

        //TODO take care of hardcoded port; here we should connect to all other processes
        client.connect(1337, '127.0.0.1', () => {

            // this.processes.forEach(process => {

            // create heartbeat message
            let heartBeatMessage = protos.Message.create();
            heartBeatMessage.type = protos.Message.Type.ELD_HEARTBEAT_;
            let eldHeartBeat = protos.EldHeartbeat_.create();
            eldHeartBeat.epoch = epoch;
            heartBeatMessage.eldHeartbeat_ = eldHeartBeat;

            //create plSend event
            let plSend = protos.PlSend.create();
            plSend.message = heartBeatMessage;

            //create sender as me
            let processId = protos.ProcessId.create();
            processId.host = process.env.HOST;
            processId.port = process.env.PORT;
            processId.owner = process.env.OWNER;
            processId.rank = process.env.RANK;
            processId.index = process.env.INDEX;

            //create plDeliver event
            let plDeliver = protos.PlDeliver.create();
            plDeliver.message = heartBeatMessage;
            plDeliver.sender = processId;

            //deliver message - global message
            let message = protos.Message.create();
            message.type = protos.Message.Type.PL_DELIVER;
            message.plSend = plSend;
            message.plDeliver = plDeliver;

            const bufferedData = Buffer.from(JSON.stringify(message));
            client.write(bufferedData, (err) => {
                client.end();
            });

            // });
        });
    };

    selectNewLeader = () => {
        let candidatesArray = Array.from(this.candidates.values());
        const minEpochCandidate = _.minBy(candidatesArray, 'epoch');
        const minCandidatesArray = candidatesArray.filter(candidate => candidate.epoch === minEpochCandidate.epoch);
        return minCandidatesArray.reduce((p, c) => p.process.rank > c.process.rank ? p : c).process;
    };

    sendTrustLeader = leader => {
        const client = new net.Socket();

        client.connect(process.env.PORT, process.env.HOST, () => {

            let message = protos.Message.create();
            message.type = protos.Message.Type.ELD_TRUST;

            let eldTrust = protos.EldTrust.create();
            eldTrust.processId = leader;

            message.eldTrust = eldTrust;
            const bufferedData = Buffer.from(JSON.stringify(message));

            client.write(bufferedData, (err) => {
                client.end();
            });
        });
    }

    replay = (leader, previousTimeoutId) =>{
        if (previousTimeoutId){
            clearTimeout(previousTimeoutId);
        }
        const timeoutId = setTimeout(() => {
            const newLeader = this.selectNewLeader();
            if (!_.isMatch(newLeader, leader)) {
                this.delay += this.CONSTANT_DELAY;
                this.sendTrustLeader(newLeader);
            }

            this.sendHeartbeatToAllProcesses();
            this.candidates = new Set();
            this.replay(leader, timeoutId);
        }, this.delay);
    }
}

module.exports = decorateWithSingleton(EldService);
