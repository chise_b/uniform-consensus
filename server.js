const emitter = require('./utils/emitter').getInstance().getEmitter();
const net = require('net');
const protos = require("./protos/model");

let args = process.argv.slice(2);
const owner = args[0];
const index = args[1];
const port = args[2];

let heartNr = 0;

PORT = port;
HOST = '127.0.0.1';

const service = require("./new-consensus/globalService").getInstance();
const app = require('./new-consensus/app').getInstance();

const server = net.createServer((socket) => {

    socket.on('data', (data) => {

        const parsedMessage = service.decode(data);
        handleMessage(parsedMessage);

    });

});

handleMessage = (receivedMessage) => {
    const networkMessage = receivedMessage.networkMessage;
    if (networkMessage) {
        if (networkMessage.message.type === protos.Message.Type.APP_PROPOSE) {
            app.appPropose(networkMessage);
            console.log('Server Received Data:' + JSON.stringify(receivedMessage));
        } else {
            let insideMessage = networkMessage.message;

            let sender = protos.ProcessId.create();
            sender.senderHost = networkMessage.senderHost;
            sender.port = networkMessage.senderListeningPort;

            let plDeliver = protos.PlDeliver.create();
            plDeliver.sender = sender;
            plDeliver.message = insideMessage;

            let message = protos.Message.create();
            message.type = protos.Message.Type.PL_DELIVER;
            message.plDeliver = plDeliver;

            if (plDeliver.message.type === protos.Message.Type.EPFD_HEARTBEAT_REQUEST){
                if (heartNr === 0) {
                    heartNr += 1
                    emitter.emit('PL-DELIVER', message);
                    console.log('Server Received Data:' + JSON.stringify(receivedMessage));
                } else if (heartNr === 50) {
                    heartNr = 0
                } else {
                    heartNr += 1
                }
            }
            else{
                emitter.emit('PL-DELIVER', message);
                console.log('Server Received Data:' + JSON.stringify(receivedMessage));
            }


        }
    }
}

server.on('listening', () => {
    console.log('Server with index ' + HOST + ' started to listen on port ' + PORT + '...\n');
    app.appRegistration(owner, index);
})

server.listen(PORT, HOST);
