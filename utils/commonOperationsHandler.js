'use strict';

const Operations = require('./Operations');

module.exports = () => {
  const operations = new Operations();

  operations.use((context, next) => {
    context.body = context.raw;

    next();
  });

  return operations;
};
