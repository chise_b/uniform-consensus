'use strict';

module.exports = (ClassImpl) => {
    ClassImpl.getInstance = function() {
        if (!ClassImpl.instance$) {
            ClassImpl.instance$ = new ClassImpl();
        }

        return ClassImpl.instance$;
    }

    return ClassImpl;
};
