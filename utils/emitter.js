const events = require('events');
const decorateWithSingleton = require('./decorateWithSingleton');

class Emitter{

    constructor() {
        this.emitter = new events.EventEmitter();
    }

    getEmitter = () => this.emitter;
}

module.exports = decorateWithSingleton(Emitter);
