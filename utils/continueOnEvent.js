'use strict';

const _ = require('lodash');

module.exports = (event) => (context, next) => {
  const isOk = _.isMatch(context.body, event);

  if (isOk) next();
};
