'use strict';

const compose = require('./compose');

class Operations {
  constructor() {
    this.handlers = [];
  }

  static fromOperations(operations) {
    const instance = new Operations();
    instance.handlers = operations ? [...operations.handlers] : [];

    return instance;
  }

  /**
   * Add a new task for execution
   * @param  {function} fn
   *
   *  tasks.use((context, next) => {
   *    // do something
   *    next().then();
   *  });
   *  tasks.use(async (context, next) => {
   *    // do something
   *    await next();
   *    // do other things
   *  });
   *  tasks.use((context) => {
   *    // done :)
   *  });
   *
   * TODO: support arrays
   */
  use(fn) {
    this.handlers.push(fn);
  }

  async handle(context) {
    const fn = compose(this.handlers);

    return fn(context)
  }
}

module.exports = Operations;
