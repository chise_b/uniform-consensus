var net = require('net');

const client = new net.Socket();

const protos = require("./protos/model");

client.connect(1337, '127.0.0.1', () => {
    console.log('Connected');

    let message = protos.Message.create();
    message.type = protos.Message.Type.APP_DECIDE;
    const bufferedData = Buffer.from(JSON.stringify(message));
    client.write(bufferedData);
});

client.on('data', function (data) {
    console.log('Received: ' + data);
    // client.destroy(); // kill client after server's response
});

client.on('close', function () {
    console.log('Connection closed');
});
